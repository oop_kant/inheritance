/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.inheritance;

/**
 *
 * @author MSI
 */
public class Duck extends Animal{
    public Duck(String name, String color){
        super(name, color, 2);
        System.out.println("Duck create");
        
    }
    public void fly(){
        System.out.println("Duck: " + name + " fly!!!");
    }
    
    @Override
    public void walk(){
        super.walk();
        System.out.println("Duck: "+ name +" walk with " + numberOfLegs + " legs.");
        
    }
    @Override
    public void speak(){
        super.speak();
        System.out.println("Duck: "+ name + " speak --> Kab Kab!!!")
        ;
    }
    
}
