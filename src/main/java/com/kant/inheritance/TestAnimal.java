/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.inheritance;

/**
 *
 * @author MSI
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang", "Black&White");
        Dog to = new Dog("To", "Orange");
        Dog mome = new Dog("Mome", "Black&White");
        Dog bat = new Dog("Bat", "Black&White");
        //dang.speak();
        //dang.walk();
        //to.speak();
        //to.walk();
        //mome.speak();
        //mome.walk();
        //bat.speak();
        //bat.walk();
        
        Cat zero = new Cat("Zero", "Orange" );
        //zero.speak();
        //zero.walk();
        
        Duck zom = new Duck("ZOM", "Orange");
        Duck gabgab = new Duck("GabGab", "Black");
        //zom.speak();
        //zom.walk();
        //zom.fly();
        //gabgab.speak();
        //gabgab.walk();
        //gabgab.fly();
        
        //System.out.println("Zom is Animal: " + (zom instanceof Animal));
        //System.out.println("Zom is Duck: " + (zom instanceof Duck));
        //System.out.println("Zom is Cat: " + (zom instanceof Object));
        //System.out.println("Animal is Dog: " + (animal instanceof Dog));
        //System.out.println("Animal is Animal: " + (animal instanceof Animal));
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom;
        ani2 = zero;
        
        //System.out.println("Ani1: zom is Duck " + (ani1 instanceof Duck));
        
        Animal[] animals = {dang ,to,mome,bat, zero , zom,gabgab};
        for(int i=0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
                
            }
        }
        
    }
}
